﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment_2a.usercontrols
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string CodeToServe
        {
            get { return (string)ViewState["CodeToServe"]; }
            set { ViewState["CodeToServe"] = value; }
        }

        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();

            debug.InnerHtml = CodeToServe;

            //First column is the line number of the code (idx -> index)
            DataColumn idx_col = new DataColumn();

            //SEcond column is the actual code itself
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            //These Tildas will be replaced with line indents
            //When the code lines are bound to rows.
            //If you have double quotes in your html code, escap them with \
            //eg. <div style=\"background:green;\"></div>
            List<string> front_end_code = new List<string>(new string[]{
                "img {",
              "~padding - bottom 5px;",
              "~padding - right 10px;",
              "}"
            });


            List<string> my_js = new List<string>(new string[] {
                "~var animal [\"Monkey\", \"Cat\", \"Dog\", \"Horse\", \"Goat\" ];",
                "~The Array I've created:",
                "~var mybooks =[book1, book2, book3, book4, book5, book6, book7, book8, book9, book10];",
                "~This code example promts the user to select a number between 1 - 10, each number which will revel a book title.",
                "~var wwyl = prompt(\"Which top 10 book would you like?\", \"Pick a number: 1-10\"); mybooks.unshift(book1);",
                "~console.log(mybooks);",
                "~mybooks.unshift(book1); < br />",
                "~console.log(mybooks);< br /> < p > Unshift adds an item to the front of the array, in this case it would be book1.</ p >",

                "~var bookchoice = mybooks[wwyl] < br />",

                "~if (isNaN(wwyl) || wwyl < 1 || wwyl > 10)< br />",
                "~{",
                "~alert(\"please enter a number between 1-10\"); (This would display the error message for the invalid entry)",
                "~}< br />",
                "~else",
                "~{",
                "~alert(\"Number On the list is \" + wwyl + bookchoice); (The user's book choice)",
                "~}< br />",
            });

            List<string> my_db= new List<string>(new string[] {
                "~Select bookname, libname",
                "~From books",
                "~inner join booksxlibrarires on books.bookid = books.libraries, = bookid<br />",
                "~inner join libraries on booksxlibraries.libraryid = library.libraryid; "
            });

            //different list of strings for each piece of code that you want to serve

         // if (CodeToServe == "my_js")
           //{
           //     code = js_code;
       //    }

     //       else if (CodeToServe == "front_end_code")
     //       {
       //         code = design_code;
         //   }

           // else if (CodeToServe == "my_db")
       //     {
         //       code = database_code;
           // }

            int i = 0;
            foreach (string code_line in my_js)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }


            DataView codeview = new DataView(codedata);
            return codeview;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();
            my_code.DataSource = ds;

            /*Some formatting in the codebehind*/


            my_code.DataBind();



        }


    
}
}